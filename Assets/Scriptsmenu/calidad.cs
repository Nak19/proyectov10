﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class calidad : MonoBehaviour
{
    public TMP_Dropdown dropdown;
    public int Calidad;

    // Start is called before the first frame update
    void Start()
    {
        Calidad = PlayerPrefs.GetInt("numerocalidad", 3);
        dropdown.value = Calidad;
        ajustarCalidad();
    }

   public void ajustarCalidad()
    {
        QualitySettings.SetQualityLevel(dropdown.value);
        PlayerPrefs.SetInt("numerocalidad", dropdown.value);
        Calidad = dropdown.value;
    }
}
