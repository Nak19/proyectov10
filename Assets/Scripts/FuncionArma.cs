﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ModoDisparo
{
    DisparoBasico
}
public class FuncionArma : MonoBehaviour
{
    protected Animator animator;
    protected AudioSource audioSource;
    public bool tiempoDisparo = false; //tiempoNoDisparo
    public bool DisparoListo = false; //PuedeDisparar
    public bool Recargando = false;

    [Header("Referencia de los objetos")]
    public ParticleSystem fuegoArma;
    public Transform puntoDisparo;
    public GameObject efectoDañoDisparo;


    [Header("Referencia de Sonidos")]
    public AudioClip SonidoDisparo;
    public AudioClip SonidoSinBalas;
    public AudioClip SonidoCargadorEntra;
    public AudioClip SonidoCargadorSale;
    public AudioClip SonidoVacio;
    public AudioClip SonidoDesenfundar;

    [Header("Atributos del arma")]
    public ModoDisparo modoDisparo = ModoDisparo.DisparoBasico;
    public float daño = 35f;
    public float TiempoEntreDisparo = 0.4f; //ritmoDeDisparo
    public int municionRestantes; //balasRestantes
    public int municion; //balasEnCartucho
    public int tamañoCargador = 10; //tamañoDeCartucho
    public int municionMaxima = 20; //MaximoDeBalas

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();

        municion = tamañoCargador;
        municionRestantes = municionMaxima;

        Invoke("HabilitarArma", 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if(modoDisparo == ModoDisparo.DisparoBasico && Input.GetButton("Fire1"))
        {
            RevisarDisparo();

        }
        if (Input.GetButtonDown("Reload"))
        {
            RevisarRecarga();
        }
    }

    void HabilitarArma()
    {
        DisparoListo = true;

    }

    void RevisarDisparo()
    {
        if (!DisparoListo) return;
        if (tiempoDisparo) return;
        if (Recargando) return;
        if (municion > 0)
        {
            Disparar();
        }
        else
        {
            SinBalas();
        }
    }

    void Disparar()
    {
        audioSource.PlayOneShot(SonidoDisparo);
        tiempoDisparo = true;
        fuegoArma.Stop();
        fuegoArma.Play();
        ReproducirAnimacionDisparo();
        municion--;
        StartCoroutine(ReiniciarTiempoDisparo());
        DisparoDirecto();
    }

    public void CrearEfectoDaño(Vector3 pos, Quaternion rot)
    {
        GameObject efectoDaño = Instantiate(efectoDañoDisparo, pos, rot);
        Destroy(efectoDaño, 1f);
    }
    void DisparoDirecto()
    {
        RaycastHit hit;
        if(Physics.Raycast(puntoDisparo.position, puntoDisparo.forward, out hit))
        {
            if(hit.transform.CompareTag("Enemigo"))
            {
                Vida vida = hit.transform.GetComponent<Vida>();
                if (vida == null)
                {
                    throw new System.Exception("El enemigo ya no tiene vida");

                }
                else 
                {
                    vida.RecibirDaño(daño);
                    CrearEfectoDaño(hit.point, hit.transform.rotation);
                }
            }
        }
    }
    public virtual void ReproducirAnimacionDisparo()
    {
        if(gameObject.name == "Police9mm")
        {
            if(municion > 1)
            {
                animator.CrossFadeInFixedTime("Fire", 0.1f);
            }
            else
            {
                animator.CrossFadeInFixedTime("FireLast", 0.1f);
            }

        }
        else
        {
            animator.CrossFadeInFixedTime("Fire", 0.1f);
        }
    }

    void SinBalas()
    {
        audioSource.PlayOneShot(SonidoSinBalas);
        tiempoDisparo = true;
        StartCoroutine(ReiniciarTiempoDisparo());
    }

    IEnumerator ReiniciarTiempoDisparo()
    {
        yield return new WaitForSeconds(TiempoEntreDisparo);
        tiempoDisparo = false;
    }

    void RevisarRecarga()
    {
        if(municionRestantes > 0 && municion < tamañoCargador)
        {
            Recargar();
        }
    }

    void Recargar()
    {
        if (Recargando) return;
        Recargando = true;
        animator.CrossFadeInFixedTime("Reload", 0.1f);
    }

    void RecargarMuniciones()
    {
        int balasParaRecargar = tamañoCargador - municion;
        int restarBalas = (municionRestantes >= balasParaRecargar) ? balasParaRecargar : municionRestantes;

        municionRestantes -= restarBalas;
        municion += balasParaRecargar;
    }

    public void CargadoOn()
    {
        audioSource.PlayOneShot(SonidoDesenfundar);
    }

    public void CargadorVacioOn()
    {
        audioSource.PlayOneShot(SonidoCargadorEntra);
        RecargarMuniciones();
    }

    public void CargadorSaleOn()
    {
        audioSource.PlayOneShot(SonidoCargadorSale);
    }

    public void ArmaListaOn()
    {
        audioSource.PlayOneShot(SonidoVacio);
        Invoke("ReiniciarRecargar", 0.1f);
    }

    void ReiniciarRecargar()
    {
        Recargando = false;
    }
}
